package tungphongdo.com.entity;

import tungphongdo.com.dto.UserRoleDTO;

import javax.persistence.*;

@Entity
@Table(name = "USER_ROLE")
public class UserRole {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "USER_ID")
    private Integer userId;

    @Column(name = "ROLE_ID")
    private Integer roleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }


    public UserRoleDTO toModel(){
        UserRoleDTO userRoleDTO = new UserRoleDTO();
        userRoleDTO.setId(this.getId());
        userRoleDTO.setUserId(this.getUserId());
        userRoleDTO.setRoleId(this.getRoleId());
        return userRoleDTO;
    }
}

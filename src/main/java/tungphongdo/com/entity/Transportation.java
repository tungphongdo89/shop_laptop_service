package tungphongdo.com.entity;

import tungphongdo.com.dto.TransportationDTO;

import javax.persistence.*;

@Entity
@Table(name = "TRANSPORTATION")
public class Transportation {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "BILL_ID")
    private Integer billId;

    @Column(name = "STATUS_BILL")
    private Integer statusBill;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getStatusBill() {
        return statusBill;
    }

    public void setStatusBill(Integer statusBill) {
        this.statusBill = statusBill;
    }

    public TransportationDTO toModel(){
        TransportationDTO transportationDTO = new TransportationDTO();
        transportationDTO.setId(this.getId());
        transportationDTO.setBillId(this.getBillId());
        transportationDTO.setStatusBill(this.getStatusBill());
        return transportationDTO;
    }
}

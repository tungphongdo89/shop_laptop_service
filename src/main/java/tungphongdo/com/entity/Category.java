package tungphongdo.com.entity;

import tungphongdo.com.dto.CategoryDTO;

import javax.persistence.*;

@Entity
@Table(name = "CATEGORY")
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "ROLE_NAME")
    private String categoryName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public CategoryDTO toModel(){
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(this.getId());
        categoryDTO.setCategoryName(this.getCategoryName());
        return categoryDTO;
    }
}

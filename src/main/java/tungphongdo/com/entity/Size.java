package tungphongdo.com.entity;

import tungphongdo.com.dto.SizeDTO;

import javax.persistence.*;

@Entity
@Table(name = "SIZE")
public class Size {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "SCREEN_SIZE")
    private Double screenSize;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(Double screenSize) {
        this.screenSize = screenSize;
    }

    public SizeDTO toModel(){
        SizeDTO sizeDTO = new SizeDTO();
        sizeDTO.setId(this.getId());
        sizeDTO.setScreenSize(this.getScreenSize());
        return sizeDTO;
    }
}

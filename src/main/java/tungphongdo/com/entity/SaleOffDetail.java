package tungphongdo.com.entity;

import tungphongdo.com.dto.SaleOffDetailDTO;

import javax.persistence.*;

@Entity
@Table(name = "SALE_OFF_DETAIL")
public class SaleOffDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "PRODUCT_DETAIL_ID")
    private Integer productDetailId;

    @Column(name = "SALE_OFF_ID")
    private Integer saleOffId;

    @Column(name = "PERCENT_REDUCING")
    private Double percenReducing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public Integer getSaleOffId() {
        return saleOffId;
    }

    public void setSaleOffId(Integer saleOffId) {
        this.saleOffId = saleOffId;
    }

    public Double getPercenReducing() {
        return percenReducing;
    }

    public void setPercenReducing(Double percenReducing) {
        this.percenReducing = percenReducing;
    }

    public SaleOffDetailDTO toModel(){
        SaleOffDetailDTO saleOffDetailDTO = new SaleOffDetailDTO();
        saleOffDetailDTO.setId(this.getId());
        saleOffDetailDTO.setProductDetailId(this.getProductDetailId());
        saleOffDetailDTO.setSaleOffId(this.getSaleOffId());
        saleOffDetailDTO.setPercenReducing(this.getPercenReducing());
        return saleOffDetailDTO;
    }
}

package tungphongdo.com.dto;

import tungphongdo.com.entity.ProductDetailComment;

public class ProductDetailCommentDTO {

    private Integer id;
    private Integer userId;
    private Integer rate;
    private String comment;
    private Integer productDetailId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRate() {
        return rate;
    }

    public void setRate(Integer rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public ProductDetailComment toModel(){
        ProductDetailComment productDetailComment = new ProductDetailComment();
        productDetailComment.setId(this.getId());
        productDetailComment.setProductDetailId(this.getProductDetailId());
        productDetailComment.setComment(this.getComment());
        productDetailComment.setRate(this.getRate());
        productDetailComment.setUserId(this.getUserId());
        return productDetailComment;
    }
}

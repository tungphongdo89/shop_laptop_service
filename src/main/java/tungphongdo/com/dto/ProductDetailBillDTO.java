package tungphongdo.com.dto;

import tungphongdo.com.entity.ProductDetailBill;

public class ProductDetailBillDTO {

    private Integer id;
    private Integer productDetailId;
    private Integer billId;
    private Integer amount;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public Integer getBillId() {
        return billId;
    }

    public void setBillId(Integer billId) {
        this.billId = billId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public ProductDetailBill toModel(){
        ProductDetailBill productDetailBill= new ProductDetailBill();
        productDetailBill.setId(this.getId());
        productDetailBill.setProductDetailId(this.getProductDetailId());
        productDetailBill.setAmount(this.getAmount());
        productDetailBill.setBillId(this.getBillId());
        return productDetailBill;
    }
}

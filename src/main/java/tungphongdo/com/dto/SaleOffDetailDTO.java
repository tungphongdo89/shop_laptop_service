package tungphongdo.com.dto;

import tungphongdo.com.entity.SaleOffDetail;

public class SaleOffDetailDTO {

    private Integer id;
    private Integer productDetailId;
    private Integer saleOffId;
    private Double percenReducing;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductDetailId() {
        return productDetailId;
    }

    public void setProductDetailId(Integer productDetailId) {
        this.productDetailId = productDetailId;
    }

    public Integer getSaleOffId() {
        return saleOffId;
    }

    public void setSaleOffId(Integer saleOffId) {
        this.saleOffId = saleOffId;
    }

    public Double getPercenReducing() {
        return percenReducing;
    }

    public void setPercenReducing(Double percenReducing) {
        this.percenReducing = percenReducing;
    }

    public SaleOffDetail toModel(){
        SaleOffDetail saleOffDetail = new SaleOffDetail();
        saleOffDetail.setId(this.getId());
        saleOffDetail.setProductDetailId(this.getProductDetailId());
        saleOffDetail.setSaleOffId(this.getSaleOffId());
        saleOffDetail.setPercenReducing(this.getPercenReducing());
        return saleOffDetail;
    }

}

package tungphongdo.com.dto;

import tungphongdo.com.entity.UserRole;

public class UserRoleDTO {
    private Integer id;
    private Integer userId;
    private Integer roleId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    public UserRole toModel(){
        UserRole userRole = new UserRole();
        userRole.setId(this.getId());
        userRole.setUserId(this.getUserId());
        userRole.setRoleId(this.getRoleId());
        return userRole;
    }

}

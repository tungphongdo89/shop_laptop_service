package tungphongdo.com.dto;

import tungphongdo.com.entity.Color;

public class ColorDTO {
    private Integer id;
    private String colorName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public Color toModel(){
        Color color = new Color();
        color.setId(this.getId());
        color.setColorName(this.getColorName());
        return color;
    }
}

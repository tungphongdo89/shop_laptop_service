package tungphongdo.com.dto;

import tungphongdo.com.entity.Category;

public class CategoryDTO {
    private Integer id;
    private String categoryName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Category toModel(){
        Category category = new Category();
        category.setId(this.getId());
        category.setCategoryName(this.getCategoryName());
        return category;
    }


}

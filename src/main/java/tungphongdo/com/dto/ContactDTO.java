package tungphongdo.com.dto;

import tungphongdo.com.entity.Contact;

public class ContactDTO {
    private Integer id;
    private String name;
    private Integer phone;
    private String comment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Contact toModel(){
        Contact contact = new Contact();
        contact.setId(this.getId());
        contact.setName(this.getName());
        contact.setPhone(this.phone);
        contact.setComment(this.comment);
        return contact;
    }
}

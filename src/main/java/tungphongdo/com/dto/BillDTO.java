package tungphongdo.com.dto;

import tungphongdo.com.entity.Bill;

import java.sql.Timestamp;

public class BillDTO {
    private Integer id;
    private Integer userId;
    private Double totalMoney;
    private Timestamp datetimeBought;
    private Integer payment;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Double getTotalMoney() {
        return totalMoney;
    }

    public void setTotalMoney(Double totalMoney) {
        this.totalMoney = totalMoney;
    }

    public Timestamp getDatetimeBought() {
        return datetimeBought;
    }

    public void setDatetimeBought(Timestamp datetimeBought) {
        this.datetimeBought = datetimeBought;
    }

    public Integer getPayment() {
        return payment;
    }

    public void setPayment(Integer payment) {
        this.payment = payment;
    }

    public Bill toModel(){
        Bill bill = new Bill();
        bill.setId(this.getId());
        bill.setUserId(this.getUserId());
        bill.setTotalMoney(this.getTotalMoney());
        bill.setDatetimeBought(this.getDatetimeBought());
        bill.setPayment(this.getPayment());
        return bill;
    }
}

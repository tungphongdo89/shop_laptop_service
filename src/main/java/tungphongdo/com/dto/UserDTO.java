package tungphongdo.com.dto;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import tungphongdo.com.entity.User;

import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

public class UserDTO {
    private Integer id;
    private String userCode;

    @NotEmpty(message = "{username.is.not.empty}")
    private String username;

    @NotEmpty(message = "{password.is.not.empty}")
    private String password;

    @NotEmpty(message = "{full.name.is.not.empty}")
    @Size(max = 100)
    private String fullName;

    @NotNull(message = "{age.is.not.empty}")
    private Integer age;

    @NotNull(message = "{gender.is.not.empty}")
    private Integer gender;

    @NotEmpty(message = "{phone.number.is.not.empty}")
    @Pattern(message = "{invalid.phone.number}", regexp = "(^$|[0-9]{10})")
    private String phoneNumber;

    @NotEmpty(message = "{email.is.not.empty}")
    @Email(message = "{invalid.email}",
            regexp = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])")
    private String email;

    @NotEmpty(message = "{address.is.not.empty}")
    private String address;
    private Integer status;
    private Integer loginFailed;

    @NotNull(message = "{gender.is.not.empty}")
    private String userRoleName;


    public List<GrantedAuthority> getAuthorities(List<RoleDTO> lisRoleDTOs) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (RoleDTO roleDTO : lisRoleDTOs) {
            authorities.add(new SimpleGrantedAuthority(roleDTO.getRoleName()));
        }
        return authorities;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getLoginFailed() {
        return loginFailed;
    }

    public void setLoginFailed(Integer loginFailed) {
        this.loginFailed = loginFailed;
    }

    public String getUserRoleName() {
        return userRoleName;
    }

    public void setUserRoleName(String userRoleName) {
        this.userRoleName = userRoleName;
    }

    public User toModel() {
        User user = new User();
        user.setId(this.getId());
        user.setUserCode(this.getUserCode());
        user.setUsername(this.getUsername());
        user.setPassword(this.getPassword());
        user.setFullName(this.getFullName());
        user.setAge(this.getAge());
        user.setGender(this.getGender());
        user.setPhoneNumber(this.getPhoneNumber());
        user.setEmail(this.getEmail());
        user.setAddress(this.getAddress());
        user.setStatus(this.getStatus());
        user.setLoginFailed(this.getLoginFailed());
        return user;
    }

    public enum UserRoleEnum {
        ADMIN(1, "ROLE_ADMIN"),
        EMPLOYEE(2, "ROLE_EMPLOYEE"),
        USER(3, "ROLE_USER");

        private final Integer roleId;
        private final String roleName;

        UserRoleEnum(Integer roleId, String roleName) {
            this.roleId = roleId;
            this.roleName = roleName;
        }

        public static Optional<UserRoleEnum> getUserRoleEnum(String roleName) {
            return Stream.of(UserRoleEnum.values()).filter(e -> e.roleName.equals(roleName)).findFirst();   // findFirst: trả về phần tử đầu tiên
        }

        public Integer getRoleId() {
            return roleId;
        }

        public String getRoleName() {
            return roleName;
        }
    }

    public enum GenderEnum {
        MALE(0, "male"),
        FEMALE(1, "female"),
        OTHER(2, "other");

        private final Integer genderCode;
        private final String genderName;

        GenderEnum(Integer genderCode, String genderName) {
            this.genderCode = genderCode;
            this.genderName = genderName;
        }

        public static Optional<GenderEnum> getGenderEnum(Integer genderCode) {
            return Stream.of(GenderEnum.values()).filter(gender -> gender.genderCode.equals(genderCode)).findFirst();
        }

        public Integer getGenderCode() {
            return genderCode;
        }

        public String getGenderName() {
            return genderName;
        }
    }
}

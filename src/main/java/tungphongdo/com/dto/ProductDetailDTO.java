package tungphongdo.com.dto;

import tungphongdo.com.entity.ProductDetail;

import java.time.LocalDateTime;

public class ProductDetailDTO {

    private Integer id;
    private Integer productId;
    private Integer sizeId;
    private Integer colorId;
    private Integer amount;
    private Double singlePrice;
    private LocalDateTime datetimeImported;
    private String description;
    private String image;

    private String productName;
    private Integer categoryId;
    private String categoryName;
    private Double screenSize;
    private String colorName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getSizeId() {
        return sizeId;
    }

    public void setSizeId(Integer sizeId) {
        this.sizeId = sizeId;
    }

    public Integer getColorId() {
        return colorId;
    }

    public void setColorId(Integer colorId) {
        this.colorId = colorId;
    }

    public Integer getAmount() {
        return amount;
    }

    public void setAmount(Integer amount) {
        this.amount = amount;
    }

    public Double getSinglePrice() {
        return singlePrice;
    }

    public void setSinglePrice(Double singlePrice) {
        this.singlePrice = singlePrice;
    }

    public LocalDateTime getDatetimeImported() {
        return datetimeImported;
    }

    public void setDatetimeImported(LocalDateTime datetimeImported) {
        this.datetimeImported = datetimeImported;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public Double getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(Double screenSize) {
        this.screenSize = screenSize;
    }

    public String getColorName() {
        return colorName;
    }

    public void setColorName(String colorName) {
        this.colorName = colorName;
    }

    public ProductDetail toModel() {
        ProductDetail productDetail = new ProductDetail();
        productDetail.setId(this.getId());
        productDetail.setProductId(this.getProductId());
        productDetail.setSizeId(this.getSizeId());
        productDetail.setColorId(this.getColorId());
        productDetail.setAmount(this.getAmount());
        productDetail.setSinglePrice(this.getSinglePrice());
        productDetail.setDatetimeImported(this.getDatetimeImported());
        productDetail.setDescription(this.getDescription());
        productDetail.setImage(this.getImage());
        return productDetail;
    }


}

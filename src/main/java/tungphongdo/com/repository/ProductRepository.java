package tungphongdo.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tungphongdo.com.entity.Product;

@Repository
public interface ProductRepository extends JpaRepository<Product, Integer> {

    @Query(value = "CALL getProductById(:productId);", nativeQuery = true)
    Product getProductById(@Param("productId") Integer productId);

}

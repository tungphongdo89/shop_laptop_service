package tungphongdo.com.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import tungphongdo.com.dto.UserRoleDTO;
import tungphongdo.com.entity.UserRole;

import java.util.List;

@Repository
public interface UserRoleRepository extends JpaRepository<UserRole, Integer> {

    @Query(value = "select * from USER_ROLE u where u.USER_ID = :userId", nativeQuery = true)
    List<UserRole> getListRolesByUserId(@Param("userId") Integer userId);

//    @Query("select new tungphongdo.com.dto.UserRoleDTO(ur.id, ur.roleId, ur.userId) from UserRole ur " +
//            "where ur.userId = ?1")
//    List<UserRoleDTO> getListRolesByUserId(int userId);
}

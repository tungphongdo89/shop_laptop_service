package tungphongdo.com.jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import tungphongdo.com.dto.RoleDTO;
import tungphongdo.com.dto.UserDTO;
import tungphongdo.com.dto.UserRoleDTO;
import tungphongdo.com.entity.Role;
import tungphongdo.com.entity.User;
import tungphongdo.com.repository.RoleRepository;
import tungphongdo.com.repository.UserRepository;
import tungphongdo.com.repository.UserRoleRepository;
import tungphongdo.com.service.JwtService;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/*Class này dùng để xác thực người dùng*/
public class JwtAuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

    private final static String TOKEN_HEADER = "authorization";

    @Autowired
    private JwtService jwtService;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest httpRequest = (HttpServletRequest) request;
        String authToken = httpRequest.getHeader(TOKEN_HEADER);
        if (jwtService.validateTokenLogin(authToken)) {
            String username = jwtService.getUsernameFromToken(authToken);
            User user = userRepository.findByUsername(username).get();


            List<RoleDTO> roleDTOs = new ArrayList<>();
            List<UserRoleDTO> listUserRoleDTOs =
                    userRoleRepository.getListRolesByUserId(user.getId()).stream().map(userRole -> userRole.toModel()).collect(Collectors.toList());

            for (UserRoleDTO userRoleDTO : listUserRoleDTOs) {
                Role role = roleRepository.getByRoleId(userRoleDTO.getRoleId()).get();
                roleDTOs.add(role.toModel());
            }

            UserDTO userDTO = user.toModel();
            List<GrantedAuthority> authorities = userDTO.getAuthorities(roleDTOs);

            if (!Objects.isNull(userDTO)) {
                boolean enabled = true;
                boolean accountNonExpired = true;
                boolean credentialsNonExpired = true;
                boolean accountNonLocked = true;
                UserDetails userDetail = new org.springframework.security.core.userdetails.User(username, userDTO.getPassword(),
                        enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetail,
                        null, userDetail.getAuthorities());
                authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
                SecurityContextHolder.getContext().setAuthentication(authentication);
            }
        }
        chain.doFilter(request, response);
    }


}

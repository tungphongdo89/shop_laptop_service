package tungphongdo.com.service.serviceImpl;

import net.bytebuddy.utility.RandomString;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import tungphongdo.com.config.MailConfig;
import tungphongdo.com.dto.UserDTO;
import tungphongdo.com.entity.User;
import tungphongdo.com.entity.UserRole;
import tungphongdo.com.exception.ResourceNotFoundException;
import tungphongdo.com.exception.SendMailException;
import tungphongdo.com.exception.ValidationException;
import tungphongdo.com.repository.UserRepository;
import tungphongdo.com.repository.UserRoleRepository;
import tungphongdo.com.resourceRequest.ChangePassRequest;
import tungphongdo.com.resourceRequest.LoginRequest;
import tungphongdo.com.resourceResponse.CommonResponse;
import tungphongdo.com.resourceResponse.GetDetailResponse;
import tungphongdo.com.resourceResponse.LoginResponse;
import tungphongdo.com.service.JwtService;
import tungphongdo.com.service.UserService;

import java.util.Date;
import java.util.Optional;

@Service
@Transactional
public class UserServiceImpl implements UserService {

    private static final Integer COUNT_LOGIN_FAILED = 4;
    private static final Integer LOGIN_FAILED_INCREASE = 1;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private JwtService jwtService;

    @Autowired
    private ResourceBundleMessageSource messageSource;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private MailConfig mailConfig;

    @Override
    public LoginResponse login(LoginRequest loginRequest) throws ResourceNotFoundException {
        User user = userRepository.findByUsername(loginRequest.getUsername())
                .orElseThrow(() -> new ResourceNotFoundException(messageSource.getMessage(
                        "account.is.not.exist", null, LocaleContextHolder.getLocale())));

        if (!BCrypt.checkpw(loginRequest.getPassword(), user.getPassword())) {
            if (user.getLoginFailed() < COUNT_LOGIN_FAILED) {
                user.setLoginFailed(user.getLoginFailed() + LOGIN_FAILED_INCREASE);
                userRepository.save(user);
                throw new ResourceNotFoundException(messageSource.getMessage(
                        "incorrect.password", null, LocaleContextHolder.getLocale()));
            } else {
                throw new ResourceNotFoundException(messageSource.getMessage(
                        "lock.account", null, LocaleContextHolder.getLocale()));
            }
        }

        LoginResponse loginResponse = new LoginResponse(new Date(),
                HttpStatus.OK.value(),
                messageSource.getMessage("login.successful", null, LocaleContextHolder.getLocale()),
                jwtService.generateTokenLogin(user.getUsername()),
                user.getFullName(),
                user.getId()
        );

        user.setLoginFailed(0);
        userRepository.save(user);

        return loginResponse;
    }

    @Override
    @Transactional
    public CommonResponse createUser(UserDTO userDTO) {
        Optional<User> user = userRepository.findByUsername(userDTO.getUsername());

        if (user.isPresent()) {
            throw new ValidationException(messageSource.getMessage("username.is.exist", null, LocaleContextHolder.getLocale()));
        }

        Optional<User> userTestForEmail = userRepository.findByEmail(userDTO.getEmail());
        if (userTestForEmail.isPresent()) {
            throw new ValidationException(messageSource.getMessage("email.is.exist", null, LocaleContextHolder.getLocale()));
        }

        userDTO.setUserCode("usercode");
        userDTO.setPassword(BCrypt.hashpw(userDTO.getPassword(), BCrypt.gensalt(12)));
        userDTO.setStatus(0);
        userDTO.setLoginFailed(0);
        User userSuccess = userRepository.save(userDTO.toModel());

        UserRole userRole = new UserRole();
        userRole.setUserId(userSuccess.getId());
        userRole.setRoleId(UserDTO.UserRoleEnum.getUserRoleEnum(userDTO.getUserRoleName()).map(u -> u.getRoleId()).orElse(1));
        userRoleRepository.save(userRole);

        return new CommonResponse(new Date(),
                HttpStatus.OK.value(),
                messageSource.getMessage("create.account.successfully", null, LocaleContextHolder.getLocale())
        );
    }

    @Override
    @Transactional
    public CommonResponse forgotPass(String email) throws ResourceNotFoundException {
        User user = userRepository.findByEmail(email)
                .orElseThrow(() -> new ResourceNotFoundException(
                        messageSource.getMessage("email.is.not.exist", null, LocaleContextHolder.getLocale()))
                );
        RandomString gen = new RandomString(8);
        String newPass = gen.nextString();

        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setTo(user.getEmail());
        mailMessage.setSubject(messageSource.getMessage("get.password", null, LocaleContextHolder.getLocale()));
        mailMessage.setText(messageSource.getMessage("your.new.password.is", null, LocaleContextHolder.getLocale()) + " " + newPass);
        try {
            mailConfig.getJavaMailSender().send(mailMessage);
        } catch (MailException ex) {
            throw new SendMailException(messageSource.getMessage("send.mail.failed", null, LocaleContextHolder.getLocale()));
        }

        String encodePass = BCrypt.hashpw(newPass, BCrypt.gensalt(12));
        user.setPassword(encodePass);
        userRepository.save(user);

        return new CommonResponse(new Date(),
                HttpStatus.OK.value(),
                messageSource.getMessage("recover.password.successful", null, LocaleContextHolder.getLocale())
        );
    }

    @Override
    @Transactional
    public CommonResponse changePass(ChangePassRequest changePassRequest) throws Exception {
        User user = userRepository.findById(changePassRequest.getId())
                .orElseThrow(() -> new Exception(messageSource.getMessage(
                        "internal.server.error", null, LocaleContextHolder.getLocale())));

        if (!BCrypt.checkpw(changePassRequest.getCurrentPass(), user.getPassword())) {
            throw new Exception(messageSource.getMessage(
                    "current.pass.is.not.correct", null, LocaleContextHolder.getLocale()
            ));
        }

        user.setPassword(BCrypt.hashpw(changePassRequest.getNewPass(), BCrypt.gensalt(12)));
        userRepository.save(user);

        return new CommonResponse(new Date(),
                HttpStatus.OK.value(),
                messageSource.getMessage("change.pass.successfully", null, LocaleContextHolder.getLocale()));
    }

    @Override
    @Transactional
    public GetDetailResponse userDetail(Integer userId) throws Exception {
        User user = userRepository.findById(userId)
                .orElseThrow(() -> new Exception(messageSource.getMessage(
                        "internal.server.error", null, LocaleContextHolder.getLocale())));

        return new GetDetailResponse(new Date(),
                HttpStatus.OK.value(),
                messageSource.getMessage("get.detail.user.success", null, LocaleContextHolder.getLocale()),
                user.toModel()
        );

    }


}

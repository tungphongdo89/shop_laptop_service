package tungphongdo.com.service;

import tungphongdo.com.dto.ProductDTO;
import tungphongdo.com.dto.ProductDetailDTO;
import tungphongdo.com.resourceRequest.GetAllProductRequest;
import tungphongdo.com.resourceRequest.SearchRequest;
import tungphongdo.com.resourceResponse.ServiceResult;

public interface ProductService {
    ServiceResult<ProductDetailDTO> getAllProduct(SearchRequest<GetAllProductRequest> productSearchRequest);
    ProductDTO getProductById(Integer productId);
}
